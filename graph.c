#include <stdio.h>
#define N 2000

static void plot(int *a, size_t len)
{
    fprintf(stdout,
        "set datafile separator ','\n"
        //"set terminal png size 1024,768\n"
        "set title '%s'\n"
        "set xlabel '%s'\n"
        "set ylabel '%s'\n"
        "set key left top\n"
        "set grid\n",
        "Fly straight, damn it!", "x", "y"
    );
    fprintf(stdout, "plot '-' with points title 'samples' pt 7\n");
    for (size_t i = 0; i < len; i++) {
        fprintf(stdout, "%zu,%d\n", i, a[i]);
    }
}

int gcd1(int a, int b) //found GCD for two numbers method Euclid iteration method
{
       while (a != b) 
       {
        if (a > b) {
            int tmp = a;
            a = b;
            b = tmp;
        }
        b = b - a;
    }
    return a;
}

int gcd01(int a, int b) //found GCD for two numbers method Euclid iteration method
{
       while (a != b) 
       {
        if (a > b) {
            int tmp = a;
            a = b;
            b = tmp;
        }
        b = b - a;
    }
    return a;
}


int gcd02(int a, int b) //found GCD for two numbers method binar iteration
{
    int nod = 1L;
    int tmp;
    if (a == 0L)
        return b;
    if (b == 0L)
        return a;
    if (a == b)
        return a;
    if (a == 1L || b == 1L)
        return 1L;
    while (a != 0 && b != 0) {
        if (((a & 1L) | (b & 1L)) == 0L) {
            nod <<= 1L;
            a >>= 1L;
            b >>= 1L;
            continue;
        }
        if (((a & 1L) == 0L) && (b & 1L)) {
            a >>= 1L;
            continue;
        }
        if ((a & 1L) && ((b & 1L) == 0L)) {
            b >>= 1L;
            continue;
        }
        if (a > b) {
            tmp = a;
            a = b;
            b = tmp;
        }
        tmp = a;
        a = (b - a) >> 1L;
        b = tmp;
    }
    if (a == 0)
        return nod * b;
    else
        return nod * a;
} 

int main(void)
{
    int a[N] = {0};

    a[0] = 1;
    a[1] = 1;

    for (int i = 2; i <= N; i++) {

        if (gcd02(a[i-1], i) == 1)
            
        a[i] = a[i-1] + i +1;
            
            else 

            a[i] = a[i-1]/gcd02(a[i-1], i);

    }

    plot(a, N);

    return 0;
}