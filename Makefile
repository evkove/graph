check: graph
	./graph | gnuplot -p

graph:
	cc -Wall -g -std=c11 graph.c -o $@
